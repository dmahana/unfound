# UnFound Assignment

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up and run this application ? ###

1. Install Node Js from https://nodejs.org/en/
2. Install Git Bash from https://git-scm.com/downloads
3. Open git bash change directory to where you want to save the folder and clone the repository by - `git clone <repo link>`
4. Make sure you have installed **Angular CLI** globally - `npm install -g @angular/cli`
5. Make sure you have installed **concurrently** node package globally - `npm install concurrently -g`
6. Open cmd and change directory to Project folder - `cd unfound` if unfound is your folder name.
7. Run - `npm install` to install node modules
8. Run - `npm start` to start server and angular app.
9. Now go to `localhost:4200` **OR** `localhost:3000` in browser to run the app.

### Contribution guidelines ###

Thanks,
Deepak Mahana
