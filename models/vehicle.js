const mongoose = require('mongoose');

// Initalize Schema
const Schema = mongoose.Schema

// Define a VehicleSchema
const VehicleSchema = new Schema({ 

    vehicleType : String,
    
    data: Object,
    
    fileLink : String

}, {
    timestamps: true
});

// Define and Export Model
const Vehicle = mongoose.model('Vehicle', VehicleSchema);
module.exports = Vehicle;