import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import {NgbTabsetConfig, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [NgbTabsetConfig]
})
export class HomeComponent implements OnInit {

  @ViewChild('vehicleDataTable') modalContent: TemplateRef<any>;

  file: any;
  xmlData: any;
  tableData: any;
  modalReference: any;
  uploadProgress = 0;
  progressType = '';
  vehicleType = '';

  constructor(private http: HttpClient,
              config: NgbTabsetConfig,
              private modalService: NgbModal,
              public authService: AuthService,
              private toastr: ToastrService,
              private router: Router ) {
    // customize default values of tabsets used by this component tree
    config.justify = 'center';
    config.type = 'pills';
  }

  ngOnInit() {
    this.authService.getAllVehicleData().subscribe((data: any) => {
      if(data.success){
        this.tableData = data.msg;
      } else {
        this.toastr.error('<span class="now-ui-icons ui-1_bell-53"></span>' + data.msg, '', {
          timeOut: 5000,
          closeButton: true,
          enableHtml: true,
          toastClass: "alert alert-success alert-with-icon",
          positionClass: 'toast-top-center'
        });
      }
    });

  }

  onFileChanged(event) {
    let fileReader = new FileReader();
    this.file = event.target.files[0];
    fileReader.onload = (event) => {
      this.xmlData = fileReader.result;
    }
    fileReader.readAsDataURL(this.file);
    console.log(this.file);
  }

  fileOpen(link) {
    window.open("http://localhost:3000/"+link, "_blank");
  }

  onCancel() {
    delete this.file;
    this.uploadProgress = 0;
  }

  onUpload() {
    const fd = new FormData();
    fd.append('xmlData', this.file);
    this.http.post('http://localhost:3000/vehicles/xmlUpload', fd, {
      reportProgress: true,
      observe: 'events'
    }).subscribe((event: any) => {
      if (event.type === HttpEventType.UploadProgress) {
        this.uploadProgress = Math.round(event.loaded / event.total * 100);
        if (this.uploadProgress <= 30) {
          this.progressType = 'warning';
        } else if (this.uploadProgress > 30 && this.uploadProgress <= 70) {
          this.progressType = 'info';
        } else {
          this.progressType = 'success';
        }
      } else if (event.type === HttpEventType.Response) {
        if (event.body.success) {
          this.xmlData = event.body.msg;
          this.onCancel();
          this.modalService.open(this.modalContent, {windowClass: 'classname'}).result.then((result) => {
            this.toastr.error('<span class="now-ui-icons ui-1_bell-53"></span>' + "Successfully Saved Your File Data.", '', {
              timeOut: 5000,
              closeButton: true,
              enableHtml: true,
              toastClass: "alert alert-success alert-with-icon",
              positionClass: 'toast-top-center'
            });
          });
          this.ngOnInit();
        } else {
          this.onCancel();
          this.toastr.error('<span class="now-ui-icons ui-1_bell-53"></span>' + event.body.msg, '', {
            timeOut: 5000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-danger alert-with-icon",
            positionClass: 'toast-top-center'
          });
        }
      } else if (event.type === HttpEventType.ResponseHeader) {
        console.log(event.statusText);
      }
    });
  }


}
