import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ValidateService } from '../services/validate.service';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  user: any;

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.loadUser();
  }

  checkUsername(username) {
    this.validateService.checkUsername(username).subscribe((result: any) => {
      if(result.success) {
      } else {
        this.toastr.warning('<span class="now-ui-icons ui-1_bell-53"></span> Username already exists! Please use a different Username', '', {
           timeOut: 5000,
           closeButton: true,
           enableHtml: true,
           toastClass: "alert alert-warning alert-with-icon",
           positionClass: 'toast-top-right'
         });
      }
    });
  }

  checkEmail(email) {
    this.validateService.checkEmail(email).subscribe((result: any) => {
      if(result.success) {
      } else {
        this.toastr.warning('<span class="now-ui-icons ui-1_bell-53"></span> Email-ID already exists! Please use a different Email-ID', '', {
         timeOut: 5000,
         closeButton: true,
         enableHtml: true,
         toastClass: "alert alert-warning alert-with-icon",
         positionClass: 'toast-top-right'
       });
      }
    });
  }

  onEditSubmit() {

    const editUserDetails = {
      user_id:  this.user._id,
      name: this.user.name,
      email: this.user.email,
      username: this.user.username,
      password: this.user.password,
    }

    console.log(editUserDetails);

    // Required Fields
    if (!this.validateService.validateRegister(editUserDetails)) {
      this.toastr.warning('<span class="now-ui-icons ui-1_bell-53"></span> Fill all fields!', '', {
         timeOut: 5000,
         closeButton: true,
         enableHtml: true,
         toastClass: "alert alert-warning alert-with-icon",
         positionClass: 'toast-top-right'
       });
    } else if (!this.validateService.validateEmail(editUserDetails.email)) {
      this.toastr.warning('<span class="now-ui-icons ui-1_bell-53"></span> Please enter valid Email-ID!', '', {
         timeOut: 5000,
         closeButton: true,
         enableHtml: true,
         toastClass: "alert alert-warning alert-with-icon",
         positionClass: 'toast-top-right'
       });
    } else {
        // Update User
        this.authService.editUser(editUserDetails).subscribe((data: any) => {
        if (data.success) {
          this.toastr.success('<span class="now-ui-icons ui-1_bell-53"></span> User Details Edited Successfully!', '', {
             timeOut: 5000,
             closeButton: true,
             enableHtml: true,
             toastClass: "alert alert-success alert-with-icon",
             positionClass: 'toast-top-right'
           });
          localStorage.setItem('user', JSON.stringify(this.user));
        } else {
          this.toastr.error('<span class="now-ui-icons ui-1_bell-53"></span> Something went wrong!', '', {
            timeOut: 5000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-danger alert-with-icon",
            positionClass: 'toast-top-right'
          });
        }
      });
    }
  }

  loadUser() {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

}
