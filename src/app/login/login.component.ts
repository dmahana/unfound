import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: any;
  Username: String = "";
  Password: String = "";
  modalReference: any;

  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
  this.Username = "";
  this.Password = "";
    if (this.authService.loggedIn()) {
      this.user = JSON.parse(localStorage.getItem('user'));
      this.router.navigateByUrl('/home');
    }
  }

  onLoginSubmit(form) {
    this.authService.showLoader.emit(true);
    const user = {
      username: form.value.username,
      password: form.value.password
    };

    this.authService.authenticateUser(user).subscribe((data: any) => {
      if (data.success) {
        this.authService.storeUserData(data.token, data.user);
        this.toastr.success('<span class="now-ui-icons ui-1_bell-53"></span> You have successfully Logged in!.', '', {
           timeOut: 5000,
           closeButton: true,
           enableHtml: true,
           toastClass: "alert alert-success alert-with-icon",
           positionClass: 'toast-top-center'
         });
         this.authService.showLoader.emit(false);
         this.router.navigateByUrl('/home');
      } else {
        this.authService.showLoader.emit(false);
        this.toastr.error('<span class="now-ui-icons ui-1_bell-53"></span> Invalid Username or Password! Please try again.', '', {
           timeOut: 5000,
           closeButton: true,
           enableHtml: true,
           toastClass: "alert alert-danger alert-with-icon",
           positionClass: 'toast-top-center'
         });
      }
    });

  }

  open(modal) {
    this.modalReference = this.modalService.open(modal);
  }

  onChangeSubmit(form) {
   const email = form.value.email;
    if (email !== null || email !== undefined || email.trim() !== '') {
      this.authService.showLoader.emit(true);
      this.authService.sendEmail(email).subscribe((data: any) => {
        if (data.success) {
          this.modalReference.close();
          this.authService.showLoader.emit(false);
          this.toastr.success('<span class="now-ui-icons ui-1_bell-53"></span>' + data.msg , '', {
            timeOut: 5000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-success alert-with-icon",
            positionClass: 'toast-top-center'
          });
        } else {
          this.authService.showLoader.emit(false);
          this.toastr.error('<span class="now-ui-icons ui-1_bell-53"></span>' + data.msg, '', {
            timeOut: 5000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-error alert-with-icon",
            positionClass: 'toast-top-center'
          });
        }
      });
    } else {
      this.toastr.error('<span class="now-ui-icons ui-1_bell-53"></span> Please Enter Email ID.', '', {
        timeOut: 5000,
        closeButton: true,
        enableHtml: true,
        toastClass: "alert alert-error alert-with-icon",
        positionClass: 'toast-top-center'
      });
    }
  }

}
