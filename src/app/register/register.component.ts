import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../services/validate.service';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onRegisterSubmit(form) {
    form.value.userPrivilege = "Admin";
    // Validate Email
    if (!this.validateService.validateEmail(form.value.email)) {
      this.toastr.warning('<span class="now-ui-icons ui-1_bell-53"></span> Please use a valid Email-ID.', '', {
         timeOut: 8000,
         closeButton: true,
         enableHtml: true,
         toastClass: "alert alert-warning alert-with-icon",
         positionClass: 'toast-top-center'
       });
      return false;
    }

    // Register User
    this.authService.registerUser(form.value).subscribe((data: any) => {
      if (data.success) {
        this.toastr.success('<span class="now-ui-icons ui-1_bell-53"></span> Registered Successfully! You can now login.', '', {
           timeOut: 5000,
           closeButton: true,
           enableHtml: true,
           toastClass: "alert alert-success alert-with-icon",
           positionClass: 'toast-top-center'
         });
         this.router.navigateByUrl('/login');

      } else {
        this.toastr.error('<span class="now-ui-icons ui-1_bell-53"></span> Something went wrong!', '', {
           timeOut: 5000,
           closeButton: true,
           enableHtml: true,
           toastClass: "alert alert-danger alert-with-icon",
           positionClass: 'toast-top-center'
         });
      }
    });

  }

}
