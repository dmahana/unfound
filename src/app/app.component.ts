import { Component, Input } from '@angular/core';
import { AuthService } from './services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @Input() showLoader: Boolean = false;

  constructor(public authService: AuthService) {
    this.authService.showLoader.subscribe((showLoader: Boolean) => {
      this.showLoader = showLoader;
    });
  }
}
