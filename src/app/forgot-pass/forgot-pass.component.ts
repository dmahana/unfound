import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-pass',
  templateUrl: './forgot-pass.component.html',
  styleUrls: ['./forgot-pass.component.scss']
})
export class ForgotPassComponent implements OnInit {

  userId: string;

  constructor(
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private authService: AuthService,
        private router: Router,
  ) {

    this.route.params.subscribe( params => {
      this.userId = params.id;
    });

  }

  ngOnInit() {
  }

  onSubmit(form) {
    const passwords = {
      pass1: form.value.pass1,
      pass2: form.value.pass2
    };

    if (passwords.pass1 !== passwords.pass2) {
      this.toastr.warning('<span class="now-ui-icons ui-1_bell-53"></span> Password Do Not Match.', '', {
        timeOut: 5000,
        closeButton: true,
        enableHtml: true,
        toastClass: "alert alert-warning alert-with-icon",
        positionClass: 'toast-top-center'
      });
    } else {

      const user = {
        id   : this.userId,
        pass : passwords.pass1
      }
      this.authService.changePass(user).subscribe((data: any) => {
        this.authService.showLoader.emit(true);
        if (data.success) {
          this.authService.showLoader.emit(false);
          this.toastr.success('<span class="now-ui-icons ui-1_bell-53"></span>' + data.msg, '', {
            timeOut: 5000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-success alert-with-icon",
            positionClass: 'toast-top-center'
          });
          this.router.navigateByUrl('/home');
        } else {
          this.toastr.error('<span class="now-ui-icons ui-1_bell-53"></span>' + data.msg, '', {
            timeOut: 5000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-error alert-with-icon",
            positionClass: 'toast-top-center'
          });
        }
      });

    }
  }

}
