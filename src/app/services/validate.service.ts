import { Injectable } from '@angular/core';
import { baseURL } from '../shared/baseurl';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ValidateService {

  private baseUri = baseURL;
  private headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  // Validate Email
  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
  }

   // Check if Any Field is Empty
   validateRegister(user) {
    if(user.name === undefined || user.email === undefined || user.username === undefined || user.password === undefined || user.userPrivilege === undefined) {
      return false;
    } else {
      return true;
    }
  }

  // Check if Username Already Exist
  checkUsername(username) {
    return this.http.post(this.baseUri + '/users/existUserName', {username : username}, { headers: this.headers });
  }
  
  // Check if Email Already Exist
  checkEmail(email) {
    return this.http.post(this.baseUri + '/users/existEmail', {email : email}, { headers: this.headers });
  }

 
}
