import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { baseURL } from '../shared/baseurl';

@Injectable()
export class AuthService {

  @Output() showLoader: EventEmitter<any> = new EventEmitter();

  authToken: any;
  user: any;
  private baseUri = baseURL;
  private headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient, public jwtHelper: JwtHelperService) { }

  registerUser(newUser) {
    return this.http.post(this.baseUri + '/users/register', newUser, { headers: this.headers });
  }

  editUser(updatedUser) {
    console.log(updatedUser);
    return this.http.put(this.baseUri + '/users/updateUser', updatedUser, { headers: this.headers });
  }

  deleteUser(userId) {
    return this.http.post(this.baseUri + '/users/delUser', {userId: userId}, { headers: this.headers });
  }

  editProfile(user) {
    return this.http.post(this.baseUri + '/users/editprofile', user, { headers: this.headers });
  }

  authenticateUser(user) {
    return this.http.post(this.baseUri + '/users/authenticate', user, { headers: this.headers });
  }

  changePass(user) {
    return this.http.post(this.baseUri + '/users/changepass', user, { headers: this.headers });
  }

  sendEmail(email) {
    return this.http.post(this.baseUri + '/users/forgetpass', {email : email}, { headers: this.headers });
  }

  getAllVehicleData() {
    return this.http.get(this.baseUri + '/vehicles/vehicleData', {headers: this.headers});
  }

  getProfile() {
    this.loadToken();
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', this.authToken);
    return this.http.get(this.baseUri + '/users/profile', { headers });
  }

  storeUserData(token, user) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loggedIn() {
    const token: string = localStorage.getItem('id_token');
    return token != null && !this.jwtHelper.isTokenExpired(token);
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

}
