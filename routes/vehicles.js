const express       =   require('express');
const router        =   express.Router();
const multer        =   require('multer');
const path          =   require('path');
const fs            =   require('fs');
const xml2js        =   require('xml2js');
const async         =   require('async');

// Import Mongoose Model
const Vehicle = require('../models/vehicle');


////////////////// FILE UPLOAD /////////////////////

// Set File Storage Destination
const storage = multer.diskStorage({
    destination: 'uploads/',
    filename: function(req, file, cb){
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

// Init Upload Variable
const upload = multer({
    storage: storage,
    fileFilter: function(req, file, cb){
        checkFileType(file, cb);
    }
}).single('xmlData');

// Check File Type
function checkFileType(file, cb) {
    //Allowed Extension
    const filetypes = /xml/;
    // Check Ext
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // Check MIME Type
    const mimetype = filetypes.test(file.mimetype);

    if(mimetype && extname){
        return cb(null, true);
    }else{
        cb('Error: XML Only!');
    }
}

// Upload User's Profile Pic
router.post('/xmlUpload', ( req, res ) => {
    
    upload(req, res, (err) => {

        if(err){
            return res.json({ success: false, msg: err});
        }else {
            if(req.file == undefined){
                console.log("No File Selected");
            }else{
                vehicle_id = req.body.vehicleId;
                filename = req.file.filename;
                filePath = 'uploads/' + filename;

                var parser = new xml2js.Parser();
                fs.readFile(filePath, function(err, data){
                    if(err){
                        return res.json({ success: false, msg: err});
                    }else{
                        var vehicleArray = [];
                        
                        parser.parseString(data, function(err, result){
                            if(err){
                                return res.json({ success: false, msg: err});
                            }else{

                                
                                const stringXML = JSON.stringify(result);
                                const jsonData = JSON.parse(stringXML);
                                
                                
                                jsonData.vehicles.vehicle.forEach(function(xmlData) {

                                    var vehicleType = null;
                                    const frameMaterial = xmlData.frame[0].material[0];
                                    const powerTrain = xmlData.powertrain[0];

                                    if(frameMaterial === 'plastic' && powerTrain === 'human'){
                                        vehicleType = 'Big Wheel';
                                    } else if (frameMaterial === 'metal' && powerTrain === 'human'){
                                        vehicleType = 'Bicycle';
                                    } else if (frameMaterial === 'metal' && powerTrain === 'internal combustion'){
                                        vehicleType = 'Motorcycle';
                                    } else if (frameMaterial === 'plastic' && powerTrain === 'bernoulli'){
                                        vehicleType = 'Hang Glider';
                                    } else {
                                        vehicleType = 'Car';
                                    }

                                    let newVehicle = new Vehicle({ 
                                        vehicleType : vehicleType,
                                        data        : xmlData,
                                        fileLink    : filePath
                                        
                                    });

                                    vehicleArray.push(function (callback){

                                        newVehicle.save((err, Vehicle) => {
                                            if(err){
                                                return callback(err);
                                            }else{
                                                callback(null, Vehicle)
                                            }
                                        });       
                                    });
                                    

                                });

                                async.parallel(vehicleArray, function(err, result){
                                    if(err) {
                                        return res.json({ success: false, msg: "Error Async"});
                                    }else{
                                        return res.json({ success: true, msg: result});
                                    }
                                });
                               
                            }
                            
                        });
                        
                    }
                    
                });

            }
        }
    });
});


// Get All Vehicle Data

router.get('/vehicleData', (req, res) => {

    Vehicle.find({}, (err, vData) => {

		if(err){
            return res.json({ success: false, msg: "Something Went Wrong" });
		}else{
			return res.json({ success: true, msg: vData });
		}
	});

});

module.exports = router;