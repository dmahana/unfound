const express   = require('express');
const router    = express.Router();
const passport  = require('passport');
const jwt       = require('jsonwebtoken');
const bcrypt    = require('bcryptjs');
const config    = require('../config/database');
const nodemailer= require('nodemailer');

// Import Mongoose Model
const User = require('../models/user');

// Register User
router.post('/register', (req, res, next) => {

    let newUser = new User({
        name            : req.body.name,
        email           : req.body.email,
        username        : req.body.username,
        password        : req.body.password,
    });

    User.addUser(newUser, (err, user) => {
        (err) ? res.json({success: false, msg: 'Failed to Create User'}) : res.json({success: true, msg: 'User Created Successfully'});
    });
});

// Authenticate User
router.post('/authenticate', (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err, user) => {
        if(err) throw err;
        if(!user) return res.json({success: false, msg: 'User not found!'});

        User.comparePassword(password, user.password, (err, isMatch) => {
            if(err) throw err;
            if(isMatch) {
                const token = jwt.sign({data: user}, config.secret, {
                    expiresIn: 604800 // 1 week
                });

                res.json({
                    success: true,
                    token: `Bearer ${token}`,
                    user: user
                });
            } else {
                return res.json({ success: false, msg: 'Wrong Password!' });
            }
        });
    });
});

// Profile Page - Protected Route
router.get('/profile', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    res.json({user: req.user});
});

// Get All Users
router.get('/allUser', (req, res) => {

    User.find({}, (err, allUsers) => {
		if(err){
			return res.json({success: false, msg: 'Internal Server Error !'});
		}else{
			return res.json({success: true, msg: allUsers});
		}
	});

});

// Edit User Details
router.put('/updateUser',(req, res) => {

    if (!req.body.user_id) {
        res.json({ success: false, msg: 'No User ID Provided' });
    }else{

        User.findById({_id: req.body.user_id}, (err, updatedUser) => {
            if(err){
                res.json({ success: false, msg: 'No User Found' });
            }else{
                updatedUser.name = req.body.name;
                updatedUser.email = req.body.email;
                updatedUser.username = req.body.username;

                if(req.body.password != updatedUser.password){
                    bcrypt.hash(req.body.password, 10, function(err, hash) {
                        updatedUser.password = hash;
                        updatedUser.save((err, update) => {

                            if(err){
                                res.json({ success: false, msg: 'User Update Failed' });
                            }
                            else{
                                res.json({ success: true, msg: 'User Updated Successfully' });
                            }

                        });
                    });
                } else {
                  updatedUser.save((err, update) => {

                      if(err){
                          res.json({ success: false, msg: 'User Update Failed' });
                      }
                      else{
                          res.json({ success: true, msg: 'User Updated Successfully' });
                      }

                  });
                }
            }

        });

    }

});


// Delete User
router.post("/delUser", (req, res) => {

	User.findByIdAndRemove({ _id: req.body.userId }, (err) => {
		if(err){
			return res.json({success: false, msg: 'Internal Server Error !'});
		} else {
			return res.json({success: true, msg: 'User Deleted Successfully !'});
		}
	});
});

// Check for Unique EmailID
router.post('/existEmail', (req, res) => {

    email = req.body.email;

    User.find({email: email}, (err, emailFound) => {

		if(emailFound.length){
            var msg = email + " Already Registered ! Try Another Email-ID.";
            return res.json({ success: false, msg: msg });
		}else{
            var msg = email + " This Email-ID is Valid."
			return res.json({ success: true, msg: msg });
		}
	});

});

// Check for Unique UserName
router.post('/existUserName', (req, res) => {

    userName = req.body.username;

    User.find({username: userName}, (err, userNameFound) => {

		if(userNameFound.length){
            var msg = userName + " Already Registered ! Try Another UserName.";
            return res.json({ success: false, msg: msg });
		}else{
            var msg = userName + " This UserName is Valid."
			return res.json({ success: true, msg: msg });
		}
	});

});

// Forget Password

// Email Settings
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
           user: 'mdeepak10.engnr@gmail.com',
           pass: 'Dmahana123'
        }
});

router.post('/forgetpass', (req, res) => {

    const email = req.body.email;


    if (email != null || email != undefined || email.trim() != '') {

        User.findOne({email: email}, (err, user) => {
            if(err){
                return res.json({ success: false, msg: 'Email ID is Not Registered !' });
            }
            else{

                console.log(user);
                if(user === null){
                    return res.json({ success: false, msg: 'Email ID is Not Registered !' });

                } else {

                    const userId = user._id;

                    const urlToChange = "http://localhost:4200/forgot-password/"+userId;
                    const from = 'mdeepak10.engnr@gmail.com';

                    const mailOptions = {
                        from: from, // sender address
                        to: email, // list of receivers
                        subject: "Unfound - Forget Password Request", // Subject line
                        html: "<p><b>Greetings from Unfound !</b></p></br> Kindly click on the link  " + urlToChange + "  to reset your password. </br>"// plain text body
                    };

                    transporter.sendMail(mailOptions, function (err, info) {
                        if(err){
                            return res.json({ success: false, msg: 'Failed To Send Email ! Try Again.' });
                        }
                        else{
                        return res.json({ success: true, msg: 'Password Reset Link Sent To Your Email Id ! Kindly Check your Mail'});
                        }
                    });

                }

            }

        });
    }else{
        return res.json({ success: false, msg: 'Please Enter Your Email Id !' });
    }

});

router.post('/changepass', (req, res) => {

    const userId = req.body.id;
    const newPass = req.body.pass;

    if (userId != null || userId != undefined || userId.trim() != '' || newPass != null || newPass != undefined || newPass.trim() != ''){

        User.findById({_id: userId}, (err, user) => {
            if(err){
                return res.json({ success: false, msg: 'Failed To Find User.' });
            }else{
                const salt = bcrypt.genSaltSync(10);
                const hash = bcrypt.hashSync(newPass, salt);
                user.password = hash;

                user.save(function(err) {
                    if (err) {
                        return res.json({ success: false, msg: 'Failed to Save New Password. Try Again.' });
                    } else {
                        return res.json({ success: true, msg: 'Password Changed Sucessfully.' });
                    }
                });

            }
        });

    }else{
        return res.json({ success: false, msg: 'Failed To Find User ID' });
    }
});

module.exports = router;
